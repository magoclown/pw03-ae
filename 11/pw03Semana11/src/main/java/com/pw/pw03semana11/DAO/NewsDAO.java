/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw03semana11.DAO;

import com.pw.pw03semana11.models.Category;
import com.pw.pw03semana11.models.News;
import com.pw.pw03semana11.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author magoc
 */
public class NewsDAO {

//CREATE TABLE `pw03`.`news` (
//`idNews` INT NOT NULL AUTO_INCREMENT,
//`title` VARCHAR(60) NULL,
//`description` TEXT NULL,
//`category` INT NULL,
//`pathImage` VARCHAR(100) NULL,
//PRIMARY KEY (`idNews`),
//UNIQUE INDEX `idNews_UNIQUE` (`idNews` ASC) VISIBLE,
//INDEX `fk_category_idx` (`category` ASC) VISIBLE,
//CONSTRAINT `fk_category_news`
//  FOREIGN KEY (`category`)
//  REFERENCES `pw03`.`category` (`idcategory`)
//  ON DELETE NO ACTION
//  ON UPDATE NO ACTION);
//DELIMITER $$
//CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertNews`(
//IN `pTitle` varchar(60),
//IN `pDescription` text,
//IN `pCategory` int,
//IN `pPathImage` varchar(100))
//BEGIN
//INSERT INTO `pw03`.`news`
//(`title`,
//`description`,
//`category`,
//`pathImage`)
//VALUES
//(pTitle,
//pDescription,
//pCategory,
//pPathImage);
//
//END$$
//DELIMITER ;
    public static int insertNews(News news) {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL InsertNews(?, ?, ?, ?);";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getDescription());
            statement.setInt(3, news.getCategory().getId());
            statement.setString(4, news.getImagePath());
            return statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
//USE `pw03`;
//DROP procedure IF EXISTS `GetNews`;
//
//DELIMITER $$
//USE `pw03`$$
//CREATE PROCEDURE `GetNews` ()
//BEGIN
//SELECT `news`.`idNews`,
//    `news`.`title`,
//    `news`.`description`,
//    `news`.`category`,
//    `news`.`pathImage`
//FROM `pw03`.`news`;
//
//END$$
//
//DELIMITER ;
//

    public static List<News> getNews() {
        List<News> news = new ArrayList<>();
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL GetNews();";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                int id = result.getInt(1);
                String title = result.getString(2);
                String description = result.getString(3);
                int idCategory = result.getInt(4);
                Category category = CategoryDAO.getCategory(idCategory);
                String pathImage = result.getString(5);
                news.add(new News(id, title, description, pathImage, category));
            }
            return news;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return news;
    }

//    USE `pw03`;
//DROP procedure IF EXISTS `GetNew`;
//
//DELIMITER $$
//USE `pw03`$$
//CREATE PROCEDURE `GetNew` (
//IN `pIdNews` int
//)
//BEGIN
//SELECT `news`.`idNews`,
//    `news`.`title`,
//    `news`.`description`,
//    `news`.`category`,
//    `news`.`pathImage`
//FROM `pw03`.`news`
//WHERE `news`.`idNews` = pIdNews;
//END$$
//
//DELIMITER ;

    public static News getNew(int idNew) {
        Connection con = null;
        try {
            con = DbConnection.getConnection();
            String sql = "CALL GetNew(?);";
            CallableStatement statement = con.prepareCall(sql);
            statement.setInt(1, idNew);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                int id = result.getInt(1);
                String title = result.getString(2);
                String description = result.getString(3);
                int idCategory = result.getInt(4);
                Category category = CategoryDAO.getCategory(idCategory);
                String pathImage = result.getString(5);
                return new News(id, title, description, pathImage, category);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        return null;
    }
}
